let Page = require('./Page.js')

class ContactPage extends Page {

  get elements() {
    return {
      "name": this.by.name('contact-name'),
      "email": this.by.name('contact-email'),
      "business": this.by.name('contact-business'),
      "mobile": this.by.name('contact-mobile'),
      "message": this.by.name('contact-message'),
      "policy": this.by.css(".check > label:nth-child(2)"),
      "submit": this.by.className('js-submit-trigger'),
    }
  }

  get url() {
    return 'https://clouddistrict.com/contacto'
  }

}

module.exports = ContactPage 
