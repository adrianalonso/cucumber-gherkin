let Page = require('./Page.js')

class HomePage extends Page {

  get elements() {

    return {
      "contact": this.by.linkText('Contacto')
    }
  
  }

  get url() {
    return 'https://clouddistrict.com/'
  }

}

module.exports = HomePage
