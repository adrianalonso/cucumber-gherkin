
# Cucumber Gherkin Example

This repository is a Gherkin and Cucumber.io example to show end to end tests.

## HOW TO RUN

1. docker-compose up
2. docker-compose exec dev-node /bin/bash
3. NODE_ENV=local TEST_CLIENT=chrome grunt test