Feature: Form Contact Feature
  As a Anonymous user
  I want to contact with Cloud District
  In order to contract a new project

  Scenario: User fill a form and new lead is sended
    Given I am on the Cloud District homepage
    When I click on contact
    And I should see Contáctanos
    And I fill name with Adrián
    And I fill email with alonsus91@gmail.com
    And I fill business with My Company
    And I fill mobile with 616364750
    And I fill message with Hello Cloud
    #And I accept policy
    And I submit contact form
    Then I should see Gracias por tu interés

