var seleniumWebdriver = require('selenium-webdriver');
var {defineSupportCode} = require('cucumber');

defineSupportCode(function({Given, When, Then}) {
  Given('I am on the Cloud District homepage', function() {
    var page = new HomePage(app.driver)
    page.resize(1200, 820);

    return page.go() 
  });

  When('I click on {string}', function (text) {
    var page = new HomePage(app.driver)
    
    return page.click(text)
  });

  Then('I should see {string}', function (text) {
    var xpath = "//*[contains(text(),'" + text + "')]";
    var page = new ContactPage(app.driver)

    return page.waitAndLocate({xpath: xpath}, 5000)
  });

  When('I fill {formName} with {formValue}', function (formName, formValue) {
    var page = new ContactPage(app.driver)    
     return page.waitAndFill(formName, formValue, 1000)
  });

  When('I submit contact form', function () {
    var page = new ContactPage(app.driver)

    page.snapShot("submit")

    return page.click("submit")
  });

  When('I accept policy', function () {
    var page = new ContactPage(app.driver)

    return page.click("policy")
  });
  
});
